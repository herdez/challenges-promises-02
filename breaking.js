/*
Given the following code:

function test() {
    var promise = job();

    promise.then(function(data) {
        doSomething(data);
    });

    return promise;
}

The problem: Breaking the chain

The result of 'promise.then' is a lost promise because no one can interact with it.


//++Constrainsts

- Define 'job()' that resolves 'result of job' (in string) after 1 second.

- Define 'doSomething()' callback to log data:

Output example:

```
Value is... result of job

```

*/


//+++ YOUR CODE GOES HERE



//test()




//job()





//doSomething()






// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*


//test promise
test(doSomething)

/* Output

Value is... result of job

*/














/*source:

- Adapted from https://www.codingame.com/playgrounds/347/javascript-promises-mastering-the-asynchronous/traps-of-promises


*/



